# Introduction

This project is using Laravel v5.7.19

## Project first time initialization

1. Copy project environment file `.env.example` or execute `cp .env.example .env`
2. Initiate APP_KEY or execute `php artisan key:generate`
3. Initiate node_modules or execute `npm install`

## Prepare before deploy

1. Compily JS and CSS resources or execute `npm run dev`. Or, while in depelopment run `npm run watch`.
2. Execute `php artisan serve`

#!/usr/bin/env python

import json
import sys
import csv

def get_source_data(source_filename, file_type = 'json'):
    """
    :param source_filename: Source filename.
    :param file_type: File type.
    :return: None or list.
    """
    retval = None
    with open(source_filename, 'r') as f:
        retval = json.load(f) if file_type == 'json' else csv.reader(f)
    return retval

def get_geojson_data(geojson_filename):
    """
    :param geojson_filename: GeoJson filename.
    :return: None or dictionary.
    """
    retval = None
    with open(geojson_filename, 'r') as f:
        retval = json.load(f)
    return retval


def process(geojson_data, source_data, relation, column_from_source=None, remove_from_geojson=None, rename_properties=None):
    """
    :param geojson_data: GeoJson data.
    :param source_data: Source data.
    :param relation: Relation dict between geojson attributes and source attributes.
    :param column_from_source: List of column from source that want to become feature properties. None if want to insert all columns.
    :param remove_from_geojson: List of properties from geojson that want to keep. None if want to keep it all.
    :return: Dict
    """
    if geojson_data is None or source_data is None or relation is None:
        raise Exception('Cannot process empty parameter')

    retval = {
        'type': geojson_data['type'],
        'crs': geojson_data['crs'],
        'features': []
    }

    source_data_by_index = {}
    for source_row in source_data:
        index = ''
        for key, value in relation.iteritems():
            if value not in source_row:
                raise Exception('Cannot generate source index')
            index += str(source_row[value]) + '|'
        source_data_by_index[index] = source_row

    for gj_feature in geojson_data['features']:
        index = ''
        for key in relation:
            if key not in gj_feature['properties']:
                raise Exception('Attribute', key, 'not in geojson attributes')
            index += str(gj_feature['properties'][key]) + '|'
        if index in source_data_by_index:
            feature = gj_feature.copy()

            if column_from_source is None:
                for key, value in source_data_by_index[index].iteritems():
                    feature['properties'][key] = value
            else:
                for key, value in source_data_by_index[index].iteritems():
                    if key in column_from_source:
                        feature['properties'][key] = value

            if remove_from_geojson is not None:
                for remove_key in remove_from_geojson:
                    del feature['properties'][remove_key]

            if rename_properties is not None:
                for original_col, desitation_col in rename_properties.iteritems():
                    feature['properties'][desitation_col] = feature['properties'][original_col]
                    del feature['properties'][original_col]

            retval['features'].append(feature)

    return retval

if __name__ == "__main__":
    arg_length = len(sys.argv)

    source_data = get_source_data(sys.argv[1])
    geojson_data = get_geojson_data(sys.argv[2])
    relation = json.loads(sys.argv[3]) if arg_length >= 4 else None
    remove_from_geojson = json.loads(sys.argv[4]) if arg_length >= 5 else None
    rename_properties = json.loads(sys.argv[5]) if arg_length >= 6 else None

    print json.dumps(
        process(geojson_data, source_data, relation, None, remove_from_geojson, rename_properties),
        separators=(',', ':')
    )

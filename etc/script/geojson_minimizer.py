#!/usr/bin/env python

import json
import sys

if __name__ == "__main__":
    retval = {}
    with open(sys.argv[1], 'r') as f:
        retval = json.load(f)
    print json.dumps(retval, separators=(',', ':'))

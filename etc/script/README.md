# Notes for using the script

## Example using `data_preprocessing.py`

```BASH
$ python etc/scripts/data_preprocessing.py {source file} {geojson file} {relation in json format (object)} {remove columns in json format (list)} {rename properties in json format (object)} > {export file}
```
#!/usr/bin/env python

import json
import sys
import csv
import re

if __name__ == "__main__":
    result = []
    with open(sys.argv[1], 'r') as csvfile:
        csvread = csv.reader(csvfile, delimiter=',')
        keys = None
        for index, row in enumerate(csvread):
            if index == 0:
                keys = row
            else:
                tmp = {}
                for i, key in enumerate(keys):
                    tmp[re.sub(r'[^\w\ \_]', '', key)] = row[i]
                result.append(tmp)

    print json.dumps(result, separators=(',', ':'))

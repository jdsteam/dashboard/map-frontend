#!/usr/bin/env python

import sys
import json
from pprint import pprint

from shapely.geometry import shape
from shapely.geometry.multipolygon import MultiPolygon, asMultiPolygon, MultiPolygonAdapter

def loadGeoJson(filename):
    with open(filename, 'r') as f:
        return json.load(f)
    raise Exception('Cannot open filename')

def reduceDetail(geojson_data):
    a_feature = geojson_data['features'][0]
    # feature = MultiPolygon([shape(a_feature['geometry']).buffer(0)])
    feature = MultiPolygon(polygons=a_feature['geometry']['coordinates'], context_type='geojson').buffer(0)
    pprint(feature.is_simple)

if __name__ == "__main__":
    geojson_data = loadGeoJson(sys.argv[1])
    reduceDetail(geojson_data)

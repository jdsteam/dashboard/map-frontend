<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JDS Dashboard Provider
    |--------------------------------------------------------------------------
    */

    // 'route_prefix' => 'map/',

    'layer_group' => [
        \App\Modules\Map\Layers\Custom\BappedaGroupLayer::class,
        \App\Modules\Map\Layers\Custom\DinasLingkunganHidupGroupLayer::class,
        \App\Modules\Map\Layers\Custom\KabupatenLayer::class,
    ]
];
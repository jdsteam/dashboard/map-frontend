import React, { Component } from 'react';

export class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = props.state || {
            show: false
        };
    }

    show(show) {
        this.setState({ show });
    }

    render() {
        const { show } = this.state;

        if (!show) {
            return null;
        }

        return (
            <div id="loading">
                <div>
                    <div>
                        <div className="card">
                            <div className="card-body">
                                <div>Sedang memproses...</div>
                                <div>
                                    <span className="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
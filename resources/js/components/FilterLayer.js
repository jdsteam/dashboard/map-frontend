import React, { Component } from 'react';

export class FilterLayer extends Component {
    constructor(props) {
        super(props);
        this.onSelectedAttribute = this.onSelectedAttribute.bind(this);
        this.onSelectedOperator = this.onSelectedOperator.bind(this);
        this.onFilterClick = this.onFilterClick.bind(this);
        this.onRemoveFilterClick = this.onRemoveFilterClick.bind(this);
        this.state = props.state || {};
    }

    onSelectedAttribute(e) {
        if ($(e.target).val() != '') {
            this.setState({
                selectedAttribute: $('option:selected', e.target)
            });
        } else {
            this.setState({
                selectedAttribute: null
            });
        }
    }

    onSelectedOperator(e) {
        if ($(e.target).val() != '') {
            this.setState({
                selectedOperator: $('option:selected', e.target)
            });
        } else {
            this.setState({
                selectedOperator: null
            });
        }
    }

    onFilterClick(e) {
        const el = $('input[type="text"]', $(e.target).closest('.collapse'));
        if (el.val() != '') {
            if ('onFilter' in this.props) {
                const { selectedAttribute, selectedOperator } = this.state;

                this.props.onFilter({
                    layerId: selectedAttribute.data('layerid'),
                    attribute: selectedAttribute.val(),
                    operator: selectedOperator.val(),
                    value: el.val()
                });
            }
            this.setState({
                selectedAttribute: null,
                selectedOperator: null
            });
        }
    }

    onRemoveFilterClick(e) {
        if ('onRemoveFilterClick' in this.props) {
            this.props.onRemoveFilterClick({
                layerId: $(e.target).data('id'),
                index: $(e.target).data('index')
            })
        }
    }

    render() {
        const { layerContainer } = this.props;

        if (layerContainer.length == 0) {
            return (
                <div className="alert alert-info" role="alert">Belum ada layer yang dipilih</div>
            );
        }

        const { selectedAttribute, selectedOperator } = this.state;

        return (
            <div id="accordion-filter">
                {layerContainer.map(layer =>
                    <div className="card" key={layer.id}>
                        <div className="card-header" id={layer.id.replace(/[^\w\d_-]/i, '-') + '_id_'}>
                            <h5 className="mb-0">
                                <button className="btn btn-link" data-toggle="collapse" data-target={'#' + layer.id.replace(/[^\w\d_-]/i, '-') + '_collapse_'} aria-expanded="false" aria-controls={layer.id.replace(/[^\w\d_-]/i, '-') + '_collapse_'}>
                                    {layer.config.config.title}
                                </button>
                            </h5>
                        </div>
                        <div id={layer.id.replace(/[^\w\d_-]/i, '-') + '_collapse_'} className="collapse" aria-labelledby={layer.id} data-parent="#accordion-filter">
                            <div className="card-body">
                                <ul className="list-group">
                                    {layer.filter && layer.filter.length > 0 && layer.filter.map((filter_item, i) => {
                                        let operatorLabel = '';
                                        switch (filter_item.operator) {
                                            case '0':
                                                operatorLabel = '==';
                                                break;
                                            case '1':
                                                operatorLabel = '>';
                                                break;
                                            case '2':
                                                operatorLabel = '<';
                                                break;
                                            case '3':
                                                operatorLabel = '>=';
                                                break;
                                            case '4':
                                                operatorLabel = '<=';
                                                break;
                                            case '5':
                                                operatorLabel = '!=';
                                                break;
                                        }
                                        return (
                                            <li key={i} className="list-group-item list-group-item-dark">
                                                <span>{filter_item.attr + ' ' + operatorLabel + ' ' + filter_item.value}</span>
                                                <button type="button" className="close" data-id={layer.id} data-index={i} onClick={this.onRemoveFilterClick}>
                                                    <span aria-hidden="true" data-id={layer.id} data-index={i}>&times;</span>
                                                </button>
                                            </li>
                                        );
                                    })}
                                </ul>
                                <div className="form-group">
                                    <label>Pilih attribute:</label>
                                    <select className="form-control" onChange={this.onSelectedAttribute}>
                                        <option></option>
                                        {layer.config.attributeTypes.map(attributeType =>
                                            <option key={layer.id + '_att-' + attributeType.label} value={attributeType.label} data-type={attributeType.type} data-layerid={layer.id}>{attributeType.label}</option>
                                        )}
                                    </select>
                                </div>
                                {selectedAttribute && (
                                    <div className="form-group">
                                        <label>Pilih operator:</label>
                                        <select className="form-control" onChange={this.onSelectedOperator}>
                                            <option></option>
                                            <option value="0">== (sama dengan)</option>
                                            <option value="1">&gt; (lebih dari)</option>
                                            <option value="2">&lt; (kurang dari)</option>
                                            <option value="3">&gt;= (lebih dari atau sama dengan)</option>
                                            <option value="4">&lt;= (kurang dari atau sama dengan)</option>
                                            <option value="5">!= (tidak sama dengan)</option>
                                        </select>
                                    </div>
                                )}
                                {selectedAttribute && selectedOperator && (
                                    <div className="form-group">
                                        <label>Masukkan nilai yang difilter:</label>
                                        <input type="text" className="form-control" placeholder="Nilai yang akan difilter"/>
                                    </div>
                                )}
                                {selectedAttribute && selectedOperator && (
                                    <button type="button" className="btn btn-primary btn-small" onClick={this.onFilterClick}>Filter</button>
                                )}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}
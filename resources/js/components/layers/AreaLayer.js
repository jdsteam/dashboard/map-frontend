import React from 'react';
import { GeoJsonLayer } from 'deck.gl';

export class AreaLayer extends GeoJsonLayer {
}

AreaLayer.defaultProps = {
    ...GeoJsonLayer.defaultProps,
    stroked: true,
    filled: true,
    getLineColor: [0, 0, 0],
    opacity: 1.0,
    getFillColor: d => {
        const total = parseInt(d.properties.jumlah_penduduk);
        if (total < 18000) {
            return [247, 252, 253, 255];
        } else if (total < 30000) {
            return [229, 245, 249, 255];
        } else if (total < 40000) {
            return [204, 236, 230, 255];
        } else if (total < 55000) {
            return [153, 216, 201, 255];
        } else if (total < 65000) {
            return [102, 194, 164, 255];
        } else if (total < 80000) {
            return [65, 174, 118, 255];
        } else if (total < 90000) {
            return [35, 139, 69, 255];
        }
        return [0, 109, 44, 255];
    },
    highlightColor: [0, 68, 27, 255],
    lineWidthMinPixels: 1,
    pickable: true,
    autoHighlight: true
}
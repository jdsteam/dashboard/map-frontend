import { GeoJsonLayer } from 'deck.gl';

export class JdsGeoJson extends GeoJsonLayer {
    constructor(props) {
        if (typeof props == 'undefined') {
            props = {};
        }

        if (!('onHover' in props) && ('tooltip' in props)) {
            props.onHover = JdsGeoJson.onHoverHandler;
        }
        if (!('onClick' in props) && ('tooltip' in props)) {
            props.onClick = JdsGeoJson.onClickHandler;
        }
        if (!('pickedColor' in props)) {
            props.pickedColor = 0;
        }
        const colorList = JdsGeoJson.getColorList();
        props.getFillColor = colorList[props.pickedColor].fill;
        props.highlightColor = colorList[props.pickedColor].highlightColor;

        super(props);
    }

    shouldUpdateState({ props, changeFlags }) {
        if (props.loading && props.loading.current.state.show) {
            props.loading.current.show(false);
        }
        return changeFlags.somethingChanged;
    }

    static onHoverHandler(info) {
        const { layer } = info;

        if (layer.props.tooltip) {
            layer.props.tooltip.current.onHoverHandler(info);
        }
    }

    static onClickHandler({ layer }) {
        if (layer.props.tooltip) {
            layer.props.tooltip.current.onClickHandler();
        }
    }

    static getColorList() {
        return [
            {
                fill: [0, 109, 44, 200],
                highlightColor: [0, 68, 27]
            },
            {
                fill: [244, 232, 66, 200],
                highlightColor: [255, 238, 0]
            }
        ];
    }
}

JdsGeoJson.defaultProps = {
    ...GeoJsonLayer.defaultProps,
    stroked: true,
    filled: true,
    getLineColor: [0, 0, 0],
    opacity: 1.0,
    lineWidthMinPixels: 1,
    pickable: true,
    autoHighlight: true
}

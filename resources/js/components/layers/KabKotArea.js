import React from 'react';
import { GeoJsonLayer } from 'deck.gl';

const GEOJSON_DATA_URL = {
    5: '/geojson/reduced/5/jabar_kabkot_2015_utm48s.min.json',
    50: '/geojson/reduced/50/jabar_kabkot_2015_utm48s.min.json',
    100: '/geojson/reduced/100/jabar_kabkot_2015_utm48s.min.json',
};

export class KabKotArea extends GeoJsonLayer {
    constructor(props) {
        if (!('data' in props)) {
            props.data = GEOJSON_DATA_URL[5];
        }

        super(props);
    }

    static respondOnHover(param) {
        const { tooltipObject, pointerX, pointerY } = param || {};
        if (tooltipObject) {
            return (
                <div id="tooltip" style={{ position: 'absolute', zIndex: 1, pointerEvents: 'none', left: pointerX, top: pointerY }}>
                    <span>
                        Provinsi: {tooltipObject.properties.nama_provinsi}<br/>
                        Kabupaten/Kota: {tooltipObject.properties.nama_kabkot}<br/>
                        Jumlah Penduduk: {tooltipObject.properties.jumlah_penduduk} jiwa
                    </span>
                </div>
            );
        }

        return undefined;
    }
}

KabKotArea.defaultProps = {
    ...GeoJsonLayer.defaultProps,
    stroked: true,
    filled: true,
    getLineColor: [0, 0, 0],
    opacity: 1.0,
    getFillColor: d => {
        const total = parseInt(d.properties.jumlah_penduduk);
        if (total < 700000) {
            return [247, 252, 253, 255];
        } else if (total < 1400000) {
            return [229, 245, 249, 255];
        } else if (total < 2100000) {
            return [204, 236, 230, 255];
        } else if (total < 2800000) {
            return [153, 216, 201, 255];
        } else if (total < 3500000) {
            return [102, 194, 164, 255];
        } else if (total < 4200000) {
            return [65, 174, 118, 255];
        } else if (total < 4900000) {
            return [35, 139, 69, 255];
        }
        return [0, 109, 44, 255];
    },
    highlightColor: [0, 68, 27, 255],
    lineWidthMinPixels: 1,
    pickable: true,
    autoHighlight: true
}

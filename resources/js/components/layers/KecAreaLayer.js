import React from 'react';
import { GeoJsonLayer } from 'deck.gl';

export class KecAreaLayer extends GeoJsonLayer {
    constructor(props) {
        if (!('data' in props)) {
            props.data = '/geojson/jabar_kec_2017_kemendagri_wgs84.json';
        }

        super(props);
    }

    static respondOnHover(param) {
        const { tooltipObject, pointerX, pointerY } = param || {};
        if (tooltipObject) {
            return (
                <div id="tooltip" style={{ position: 'absolute', zIndex: 1, pointerEvents: 'none', left: pointerX, top: pointerY }}>
                    <span>
                        Provinsi: {tooltipObject.properties.PROVINSI}<br />
                        Kabupaten/Kota: {tooltipObject.properties.KABKOT}<br />
                        Kecamatan: {tooltipObject.properties.KECAMATAN}<br />
                        Jumlah Penduduk: {tooltipObject.properties.jumlah_penduduk} jiwa
                    </span>
                </div>
            );
        }

        return undefined;
    }
}

KecAreaLayer.defaultProps = {
    ...GeoJsonLayer.defaultProps,
    stroked: true,
    filled: true,
    getLineColor: [0, 0, 0],
    opacity: 1.0,
    getFillColor: d => {
        const total = parseInt(d.properties.jumlah_penduduk);
        if (total < 18000) {
            return [247, 252, 253, 255];
        } else if (total < 30000) {
            return [229, 245, 249, 255];
        } else if (total < 40000) {
            return [204, 236, 230, 255];
        } else if (total < 55000) {
            return [153, 216, 201, 255];
        } else if (total < 65000) {
            return [102, 194, 164, 255];
        } else if (total < 80000) {
            return [65, 174, 118, 255];
        } else if (total < 90000) {
            return [35, 139, 69, 255];
        }
        return [0, 109, 44, 255];
    },
    highlightColor: [0, 68, 27, 255],
    lineWidthMinPixels: 1,
    pickable: true,
    autoHighlight: true
}

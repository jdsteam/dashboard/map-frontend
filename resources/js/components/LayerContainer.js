import React, { Component } from 'react';

export class LayerContainer extends Component
{
    constructor(props) {
        super(props);
        this.__renderGeoJson = this.__renderGeoJson.bind(this);
        this.onRemoveLayerClick = this.onRemoveLayerClick.bind(this);
        this.onLayerColorPickChange = this.onLayerColorPickChange.bind(this);
    }

    onRemoveLayerClick(e) {
        if ('onRemoveLayerClick' in this.props) {
            const layerId = $(e.target).data('id');
            this.props.onRemoveLayerClick(layerId);
        }
    }

    onLayerColorPickChange(e) {
        if ('onLayerColorPickChange' in this.props) {
            const layerId = $(e.target).data('id');
            this.props.onLayerColorPickChange(layerId, parseInt(e.target.value));
        }
    }

    __renderGeoJson(layer) {
        return (
            <div id={layer.id.replace(/[^\w\d_-]/i, '_') + '_collapse'} className="collapse" aria-labelledby={layer.id} data-parent="#accordion">
                <div className="card-body">
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label htmlFor={layer.id.replace(/[^\w\d_-]/i, '_') + '_selection'}>Pilih Warna Background</label>
                                <select className="form-control" data-id={layer.id} value={layer.config.config.pickedColor} onChange={this.onLayerColorPickChange} id={layer.id.replace(/[^\w\d_-]/i, '_') + '_selection'}>
                                    <option value="0">Warna Hijau</option>
                                    <option value="1">Warna Kuning</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    __renderIcon(layer) {
        return (
            <div id={layer.id.replace(/[^\w\d_-]/i, '_') + '_collapse'} className="collapse" aria-labelledby={layer.id} data-parent="#accordion">
                <div className="card-body">
                    No configuration can be made for Icon type layer
                </div>
            </div>
        );
    }

    render() {
        const {layerContainer} = this.props;

        return (
            <div id="accordion">
                {layerContainer.map(layer =>
                    <div className="card" key={layer.id}>
                        <div className="card-header" id={layer.id.replace(/[^\w\d_-]/i, '_') + '_id'}>
                            <h5 className="mb-0">
                                <button className="btn btn-link" data-toggle="collapse" data-target={'#' + layer.id.replace(/[^\w\d_-]/i, '_') + '_collapse'} aria-expanded="false" aria-controls={layer.id.replace(/[^\w\d_-]/i, '_') + '_collapse'}>
                                    {layer.config.config.title}
                                </button>
                                <button type="button" className="close" data-id={layer.id} onClick={this.onRemoveLayerClick}>
                                    <span aria-hidden="true" data-id={layer.id}>&times;</span>
                                </button>
                            </h5>
                        </div>

                        {layer.config.type == 'geojson' && this.__renderGeoJson(layer)}
                        {layer.config.type == 'icon-layer' && this.__renderIcon(layer)}
                    </div>
                )}
            </div>
        );
    }
}
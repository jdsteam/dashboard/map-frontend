import React, { Component } from 'react';
import { LayerContainer } from './LayerContainer';
import { FilterLayer } from './FilterLayer';

export class MapPanel extends Component {
    constructor(props) {
        super(props);
        this.onButtonClick = this.onButtonClick.bind(this);
        this.onRemoveLayerClick = this.onRemoveLayerClick.bind(this);
        this.onAddLayerButtonClick = this.onAddLayerButtonClick.bind(this);
        this.onLayerColorPickChange = this.onLayerColorPickChange.bind(this);
        this.onMapSelectionChange = this.onMapSelectionChange.bind(this);
        this.onFilter = this.onFilter.bind(this);
        this.onRemoveFilterClick = this.onRemoveFilterClick.bind(this);
        this.state = {selectedMap: 'KabKotArea'};
    }

    _defaultEventParameters() {
        return {
            button: 'click'
        };
    }

    onMapSelectionChange(e) {
        if ('onMapSelectionChange' in this.props) {
            this.props.onMapSelectionChange(e.target.value, e);
            this.setState({
                selectedMap: e.target.value
            });
        }
    }

    onLayerColorPickChange(layerId, value) {
        if ('onLayerColorPickChange' in this.props) {
            this.props.onLayerColorPickChange(layerId, value);
        }
    }

    onRemoveLayerClick(layerId) {
        if ('onRemoveLayerClick' in this.props) {
            this.props.onRemoveLayerClick(layerId);
        }
    }

    onButtonClick(e) {
        if ('onButtonClick' in this.props) {
            this.props.onButtonClick(e, this._defaultEventParameters());
        }
    }

    onAddLayerButtonClick(e) {
        if ('onAddLayerButtonClick' in this.props) {
            this.props.onAddLayerButtonClick(e, this._defaultEventParameters());
        }
    }

    onFilter(params) {
        if ('onFilter' in this.props) {
            this.props.onFilter(params);
        }
    }

    onRemoveFilterClick(params) {
        if ('onRemoveFilterClick' in this.props) {
            this.props.onRemoveFilterClick(params);
        }
    }

    componentDidMount() {
        axios.get('/api/map/get-list')
            .then(response => this.setState({ list: response.data }));
    }

    render() {
        const {list} = this.state;
        const {layerContainer} = this.props;

        return (
            <div className="map-panel">
                <div className="card">
                    <div className="card-header">
                        <ul className="nav nav-tabs card-header-tabs">
                            <li className="nav-item">
                                <a id="layer-panel-tab" className="nav-link active" data-toggle="tab" href="#layer-panel" role="tab" aria-controls="layer-panel" aria-selected="true">Layer</a>
                            </li>
                            <li className="nav-item">
                                <a id="filter-panel-tab" className="nav-link" data-toggle="tab" href="#filter-panel" role="tab" aria-controls="filter-panel" aria-selected="true">Filter</a>
                            </li>
                        </ul>
                    </div>
                    <div className="card-body tab-content">
                        <div className="tab-pane fade show active" id="layer-panel" role="tabpanel" aria-labelledby="layer-panel-tab">
                            {'layerNotFound' in this.props && this.props.layerNotFound && (<div className="alert alert-danger" role="alert">Layer tidak ditemukan</div>)}
                            {/* <h5 className="card-title">Special title treatment</h5> */}
                            {list && (
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="layerSelection">Pilih layer:</label>
                                        <select className="form-control" value={this.state.selectedMap} onChange={this.onMapSelectionChange} id="layerSelection">
                                            <option></option>
                                            {Object.keys(list).map(groupKey => {
                                                if (list[groupKey].layers) {
                                                    return (
                                                        <optgroup key={groupKey} label={list[groupKey].label}>
                                                            {Object.keys(list[groupKey].layers).map(layerKey =>
                                                                <option key={groupKey + '/' + layerKey} value={groupKey + '/' + layerKey}>{list[groupKey].layers[layerKey].label}</option>
                                                            )}
                                                        </optgroup>
                                                    );
                                                }
                                                return (
                                                    <option key={groupKey} value={groupKey}>{list[groupKey].label}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <button className="btn btn-primary" type="button" onClick={this.onAddLayerButtonClick}>Tambah Layer</button>
                                </form>
                            ) || (
                                <div className="alert alert-info" role="alert">Sedang memuat konfigurasi...</div>
                            )}

                            {layerContainer && layerContainer.length > 0 && (
                                <LayerContainer
                                    layerContainer={layerContainer}
                                    onRemoveLayerClick={this.onRemoveLayerClick}
                                    onLayerColorPickChange={this.onLayerColorPickChange}
                                />
                            )}
                            {/* <p className="card-text">With supporting text below as a natural lead-in to additional content.</p> */}
                            {/* <button className="btn btn-primary" onClick={this.onButtonClick}>Toggle layer</button> */}
                        </div>
                        <div className="tab-pane fade" id="filter-panel" role="tabpanel" aria-labelledby="filter-panel-tab">
                            {layerContainer && (
                                <FilterLayer
                                    layerContainer={layerContainer}
                                    onFilter={this.onFilter}
                                    onRemoveFilterClick={this.onRemoveFilterClick}
                                    />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
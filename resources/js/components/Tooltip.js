import React, { Component } from 'react';
import { JdsIconLayer, JdsGeoJson } from './layers';

const stopPropagation = evt => evt.stopPropagation();

export class JdsTooltip extends Component {
    constructor(props) {
        super(props);
        this._closePopup = this._closePopup.bind(this);
        this.state = props.state || {};
    }

    onHoverHandler(info) {
        if (this.state.expanded) {
            return;
        }

        const { x, y, object, layer } = info;
        const z = info.layer.state.z;
        const title = info.layer.props.title;

        let hoveredItems = null;
        let type = null;

        if (object) {
            if (layer instanceof JdsIconLayer) {
                hoveredItems = object.zoomLevels[z].points;
            } else if(layer instanceof JdsGeoJson) {
                hoveredItems = [{id: object.id, properties: object.properties}];
                type = 'geo';
            }
        }

        this.setState({ title, x, y, hoveredItems, type, expanded: false });
    }

    _onPopupLoad(ref) {
        if (ref) {
            // React events are triggered after native events
            ref.addEventListener('wheel', stopPropagation);
        }
    }

    _closePopup() {
        this.setState({ expanded: false, hoveredItems: null });
    }

    onClickHandler() {
        this.setState({ expanded: true });
    }

    render() {
        const { title, x, y, hoveredItems, expanded, type } = this.state;

        if (!hoveredItems) {
            return null;
        }

        if (expanded) {
            return this._renderExpanded(title, x, y, hoveredItems, type);
        }

        if (type == 'geo') {
            return this._renderGeo(title, x, y, hoveredItems);
        }

        if (hoveredItems.length > 1) {
            return (
                <div id="tooltip" style={{ left: x, top: y }}>
                    <div className="hover-tips">Ada {hoveredItems.length} {title}</div>
                </div>
            );
        }

        return (
            <div id="tooltip" style={{ left: x, top: y }}>
                <div className="hover-title"><span>{title}</span><div>{hoveredItems[0].label}</div></div>
            </div>
        );
    }

    _renderGeo(title, x, y, hoveredItems) {
        return (
            <div id="tooltip" style={{ left: x, top: y }}>
                <div className="hover-title"><span>{title}</span><div>{hoveredItems[0].properties.label}</div></div>
            </div>
        );
    }

    _renderExpanded(title, x, y, hoveredItems, type) {
        if (type == 'geo') {
            return this._renderExpandedGeo(title, x, y, hoveredItems);
        }

        if (hoveredItems.length == 1) {
            const hoveredItem = hoveredItems[0];

            return (
                <div
                    id="tooltip"
                    className="interactive"
                    ref={this._onPopupLoad}
                    style={{ left: x, top: y }}
                    onMouseLeave={this._closePopup}
                >
                    <span>{title}</span>
                    <h5>{hoveredItem.label}</h5>
                    <div className="items">
                        {Object.keys(hoveredItem.attributes).map(key =>
                            <div className="item" key={key}>{key}: {hoveredItem.attributes[key]}</div>
                        )}
                    </div>
                </div>
            );
        }

        return (
            <div
                id="tooltip"
                className="interactive multiple"
                ref={this._onPopupLoad}
                style={{ left: x, top: y }}
                onMouseLeave={this._closePopup}
            >
                <h4>{title}</h4>
                <div className="list">
                    {hoveredItems.map(({ id, label, attributes }) => {
                        return (
                            <div key={id}>
                                <h5>{label}</h5>
                                <div className="items">
                                    {Object.keys(attributes).map(key =>
                                        <div className="item" key={id + '_' + key}>{key}: {attributes[key]}</div>
                                    )}
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }

    _renderExpandedGeo(title, x, y, hoveredItems) {
        const hoveredItem = hoveredItems[0];

        return (
            <div
                id="tooltip"
                className="interactive"
                ref={this._onPopupLoad}
                style={{ left: x, top: y }}
                onMouseLeave={this._closePopup}
            >
                <span>{title}</span>
                <h5>{hoveredItem.properties.label}</h5>
                <div className="items">
                    {Object.keys(hoveredItem.properties).map(key => {
                        if (key != 'label' && key.charAt(0) == key.charAt(0).toUpperCase()) {
                            return (
                                <div className="item" key={key}>{key}: {hoveredItem.properties[key]}</div>
                            );
                        }
                        return null;
                    })}
                </div>
            </div>
        );
    }
}
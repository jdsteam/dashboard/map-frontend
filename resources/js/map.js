import React, { Component } from 'react';
import { render } from 'react-dom';
import { StaticMap } from 'react-map-gl';
import DeckGL, { MapView } from 'deck.gl';

import { WjdsController } from './components/controllers';
import { JdsIconLayer, JdsGeoJson } from './components/layers';
import { MapPanel, JdsTooltip, Loading } from './components';

const INITIAL_VIEW_STATE = {
    latitude: -6.762012391546831,
    longitude: 106.75250492485529,
    zoom: 7.5,
    bearing: 0,
    pitch: 0
};

const MAPBOX_TOKEN = document.head.querySelector('meta[name="mapbox-token"]').content; // eslint-disable-line

class DeckApp extends Component {
    constructor(props) {
        super(props);
        this.selectLayer = this.selectLayer.bind(this);
        this.onRemoveLayerClick = this.onRemoveLayerClick.bind(this);
        this.addLayerToContainer = this.addLayerToContainer.bind(this);
        this.onLayerColorPickChange = this.onLayerColorPickChange.bind(this);
        this.onFilter = this.onFilter.bind(this);
        this.onRemoveFilterClick = this.onRemoveFilterClick.bind(this);
        this._renderLayers = this._renderLayers.bind(this);
        this._tooltipRef = React.createRef();
        this._loading = React.createRef();
        this.state = props.state || {
            layerContainer: []
        };
    }

    selectLayer(value) {
        this._loading.current.show(true);
        axios.get('/api/map/layer-config/' + value)
            .then(response => this.setState({
                identifier: value,
                newLayerConfig: response.data,
                layerNotFound: false
            }))
            .catch(() => {
                this.setState({
                    newLayerConfig: null,
                    layerNotFound: true
                });
                this._loading.current.show(false);
            });
    }

    addLayerToContainer() {
        const {layerContainer, identifier, newLayerConfig} = this.state;

        if (this.__notHasIdentifier(layerContainer, identifier)) {
            layerContainer.push({
                id: identifier,
                originalConfig: newLayerConfig,
                config: newLayerConfig
            });

            this.setState({
                layerContainer: layerContainer
            });
        }
    }

    onRemoveLayerClick(layerId) {
        const { layerContainer } = this.state;
        const newLayerContainer = [];

        layerContainer.map(layer => {
            if (layer.id != layerId) {
                newLayerContainer.push(layer);
            }
        });

        this.setState({
            layerContainer: newLayerContainer
        });
    }

    onLayerColorPickChange(layerId, value) {
        const { layerContainer } = this.state;
        const newLayerContainer = [];

        layerContainer.map(layer => {
            if (layer.id != layerId) {
                newLayerContainer.push(layer);
            } else {
                layer.config.config.pickedColor = value;
                newLayerContainer.push(layer);
            }
        });

        this.setState({
            layerContainer: newLayerContainer
        });
    }

    onFilter(params) {
        const { layerContainer } = this.state;
        const newLayerContainer = [];

        layerContainer.map(layer => {
            if (layer.id != params.layerId) {
                newLayerContainer.push(layer);
            } else {
                if (typeof layer.filter == 'undefined') {
                    layer.filter = [];
                }
                layer.filter.push({
                    attr: params.attribute,
                    operator: params.operator,
                    value: params.value
                })
                layer.config.config.data = DeckApp.reformatDataSourceUrl(layer.originalConfig.config, layer.filter);
                newLayerContainer.push(layer);
            }
        });

        this._loading.current.show(true);
        this.setState({
            layerContainer: newLayerContainer
        });
    }

    static reformatDataSourceUrl(originalConfig, filter) {
        const param_array = [];

        filter.map(filter_item => {
            param_array.push('attr[]=' + filter_item.attr);
            param_array.push('op[]=' + filter_item.operator);
            param_array.push('v[]=' + filter_item.value);
        });

        return encodeURI(originalConfig.data + '?' + param_array.join('&'));
    }

    onRemoveFilterClick({ layerId, index }) {
        const { layerContainer } = this.state;
        const newLayerContainer = [];

        layerContainer.map(layer => {
            if (layer.id != layerId) {
                newLayerContainer.push(layer);
            } else {
                layer.filter.splice(index, 1);
                layer.config.config.data = DeckApp.reformatDataSourceUrl(layer.originalConfig.config, layer.filter);
                newLayerContainer.push(layer);
            }
        });

        this._loading.current.show(true);
        this.setState({
            layerContainer: newLayerContainer
        });
    }

    _prepareViews() {
        return new MapView({
            id: 'basemap', controller: WjdsController
        });
    }

    _createNewLayer(type, config) {
        switch (type) {
            case 'icon-layer':
                return new JdsIconLayer({
                    ...config,
                    tooltip: this._tooltipRef,
                    loading: this._loading
                });
            case 'geojson':
                return new JdsGeoJson({
                    ...config,
                    tooltip: this._tooltipRef,
                    loading: this._loading
                })
        }
        console.error('Layer type not found');
    }

    _renderLayers() {
        const layers = [];
        const {identifier, newLayerConfig, layerContainer} = this.state;

        layerContainer.map(layer => {
            layers.push(this._createNewLayer(layer.config.type, layer.config.config));
        });

        if (newLayerConfig && this.__notHasIdentifier(layerContainer, identifier)) {
            layers.push(this._createNewLayer(newLayerConfig.type, newLayerConfig.config));
        }

        return layers;
    }

    __notHasIdentifier(layerContainer, identifier) {
        let retval = true;

        layerContainer.map(layer => {
            if (retval && layer.id == identifier) {
                retval = false;
            }
        });

        return retval;
    }

    render() {
        const {viewState} = this.props;

        return (
            <div>
                <DeckGL
                    initialViewState={INITIAL_VIEW_STATE}
                    controller={true}
                    viewState={viewState}
                    views={this._prepareViews()}
                    layers={this._renderLayers()}
                    width="100%"
                    height="100%">

                    <StaticMap
                        reuseMaps
                        mapboxApiAccessToken={MAPBOX_TOKEN}
                        preventStyleDiffing={true}
                        mapStyle="mapbox://styles/mapbox/light-v9"
                    />

                    <JdsTooltip ref={this._tooltipRef}></JdsTooltip>

                </DeckGL>

                <MapPanel
                    onMapSelectionChange={this.selectLayer}
                    onAddLayerButtonClick={this.addLayerToContainer}
                    onRemoveLayerClick={this.onRemoveLayerClick}
                    onLayerColorPickChange={this.onLayerColorPickChange}
                    layerContainer={this.state.layerContainer}
                    layerNotFound={this.state.layerNotFound}
                    onFilter={this.onFilter}
                    onRemoveFilterClick={this.onRemoveFilterClick}
                />

                <Loading ref={this._loading}></Loading>
            </div>
        );
    }
}

render(<DeckApp />, document.getElementById('map-container'));

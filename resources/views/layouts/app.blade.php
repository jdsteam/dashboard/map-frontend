<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>WJDS - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
@section('sidebar')
    This is the master sidebar.
@show

<div class="container">
    @yield('content')
</div>

<script src="/js/manifest.js"></script>
<script src="/js/vendor.js"></script>
<script src="/js/app.js"></script>
</body>
</html>

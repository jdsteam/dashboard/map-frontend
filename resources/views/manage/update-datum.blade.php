<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.manage')

@section('title', 'Manage ' . $layer->getLabel())

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Manage Layer <strong><u>{{ $layer->getLabel() }}</u></strong></h1>

    <a href="{{ url()->route('dashboard.manage.update', ['slug' => $layer->getName()]) }}">Kembali</a>
    <!-- <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
        </button>
    </div> -->
</div>

<div class="card">
    <div class="card-header">Detail Datum</div>
    <div class="card-body">
        <form method="post" class="table-responsive">
            @csrf
            <div class="form-group">
                <label for="judulDatum">Judul Datum</label>
                <input name="detail[name]" type="text" class="form-control" id="judulDatum" placeholder="Judul datum" value="{{ $datumLayer->name }}">
            </div>
            <div class="form-group">
                <label for="judulDatum">Tipe</label>
                <select name="detail[type]" class="form-control" value="{{ $datumLayer->type }}">
                    @foreach (\App\Modules\Map\Models\DataLayer::getTypeLabels() as $typeId => $typeLabel)
                        <option value="{{ $typeId }}" {{ $typeId == $datumLayer->type ? 'selected' : '' }}>{{ $typeLabel }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="judulDatum">Status</label>
                <select name="detail[is_active]" class="form-control" value="{{ $datumLayer->is_active }}">
                    <option value="1" {{ $datumLayer->is_active ? 'selected' : '' }}>Aktif</option>
                    <option value="0" {{ $datumLayer->is_active ? '' : 'selected' }}>Non Aktif</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit Pembaharuan</button>
        </form>
    </div>
</div>
<div>&nbsp;</div>
<div class="card"><div class="card-body">
<form method="post" class="table-responsive">
    @csrf
    <table data-editable data-editable-spy class="table table-striped table-sm">
        <thead>
            <tr>
            <th>#</th>
            <th>Nama Atribut</th>
            <th>Tipe Atribut</th>
            <th>Nilai</th>
            <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attributes as $attribute)
            <tr>
                <td>{{ $attribute->id }}</td>
                <td>
                    <div class="form-group">
                        <input class="form-control" type="text" name="attribute[{{ $attribute->id }}][name]" value="{{ $attribute->getLabel() }}">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select class="form-control" name="attribute[{{ $attribute->id }}][type]" value="{{ $attribute->type }}">
                        @foreach (\App\Modules\Map\Models\DataAttributeLayer::getTypeLabels() as $typeId => $typeLabel)
                            <option value="{{ $typeId }}" {{ $typeId == $attribute->type ? 'selected' : '' }}>{{ $typeLabel }}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" name="attribute[{{ $attribute->id }}][value]" value="{{ $attribute->getValue() }}">
                    </div>
                </td>
                <td><a href="{{ url()->route('dashboard.manage.delete_attribute_datum', ['slug' => $layer->getName(), 'datumId' => $datumLayer->id, 'attributeId' => $attribute->id]) }}">Hapus</a></td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" name="newAttribute[name][]">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select name="newAttribute[type][]" class="form-control">
                        @foreach (\App\Modules\Map\Models\DataAttributeLayer::getTypeLabels() as $typeId => $typeLabel)
                            <option value="{{ $typeId }}">{{ $typeLabel }}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input class="form-control" type="text" name="newAttribute[value][]">
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <button type="submit" class="btn btn-primary">Submit Pembaharuan</button>
</form>
</div></div>
<div>&nbsp;</div>
@endsection

@push('scripts')
<script>

</script>
@endpush

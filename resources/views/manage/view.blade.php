<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.manage')

@section('title', 'Manage')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Manage Layer</h1>

    <!-- <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
        </button>
    </div> -->
</div>

<form method="post" class="table-responsive">
    @csrf
    <table data-editable data-editable-spy class="table table-striped table-sm">
        <thead>
            <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Slug</th>
            <th>Tipe Layer</th>
            <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($layers as $layer)
            <tr>
                <td>{{$layer->id}}</td>
                <td>{{$layer->getLabel()}}</td>
                <td>{{$layer->getName()}}</td>
                <td>{{$layer->getTypeLabel()}}</td>
                <td><a href="{{url()->route('dashboard.manage.update', ['slug' => $layer->getName()])}}">Update</a></td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" name="newLayer[name][]" placeholder="Nama layer baru">
                    </div>
                </td>
                <td><div class="form-group">
                        <input type="text" class="form-control" name="newLayer[slug][]" placeholder="Slug layer baru">
                    </div></td>
                <td>
                    <div class="form-group">
                        <select class="form-control" name="newLayer[type][]">
                        @foreach (\App\Modules\Map\Models\Layer::getTypeLabels() as $typeId => $typeLabel)
                            <option value="{{ $typeId }}">{{ $typeLabel }}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <button type="submit" class="btn btn-primary">Submit Pembaharuan</button>
</form>
@endsection

<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.manage')

@section('title', 'Manage ' . $layer->getLabel())

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Manage Layer <strong><u>{{ $layer->getLabel() }}</u></strong></h1>

    <a href="{{ url()->route('dashboard.manage.view') }}">Kembali</a>
    <!-- <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
        </button>
    </div> -->
</div>

<div class="card">
    <div class="card-header">Detail Layer</div>
    <div class="card-body">
        <form method="post" class="table-responsive">
            @csrf
            <div class="form-group">
                <label for="judulLayer">Judul Layer</label>
                <input name="detail[name]" type="text" class="form-control" id="judulLayer" placeholder="Judul layer" value="{{ $layer->name }}">
            </div>
            <div class="form-group">
                <label for="slug">Slug</label>
                <input name="detail[slug]" type="text" class="form-control" id="slug" placeholder="Judul layer" value="{{ $layer->slug }}">
            </div>
            <div class="form-group">
                <label for="judulDatum">Tipe</label>
                <select name="detail[type]" class="form-control" value="{{ $layer->type }}">
                    @foreach (\App\Modules\Map\Models\Layer::getTypeLabels() as $typeId => $typeLabel)
                        <option value="{{ $typeId }}" {{ $typeId == $layer->type ? 'selected' : '' }}>{{ $typeLabel }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit Pembaharuan</button>
        </form>
    </div>
</div>
<div>&nbsp;</div>
<div class="card">
    <div class="card-header">Upload Data Baru</div>
    <div class="card-body">
        <form method="post" action="{{ url()->route('dashboard.manage.add-data', ['slug' => $layer->slug]) }}" class="table-responsive" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="new-data-file">File baru</label>
                <input type="file" class="form-control-file" id="new-data-file" name="datafile" accept="text/csv">
            </div>
            <button type="submit" class="btn btn-primary">Submit Data Baru</button>
        </form>
    </div>
</div>
<div>&nbsp;</div>
<div class="card">
    <div class="card-header">Data Layer</div>
    <div class="card-body">
        <form method="post" action="{{ url()->current() }}" class="table-responsive">
            @csrf
            <table data-editable data-editable-spy class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Judul Datum</th>
                    <th>Tipe</th>
                    <th>Status</th>
                    <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataLayer as $datumLayer)
                    <tr>
                        <td>{{ $datumLayer->id }}</td>
                        <td>{{ $datumLayer->getLabel() }}</td>
                        <td>{{ $datumLayer->getTypeLabel() }}</td>
                        <td>{{ $datumLayer->is_active ? 'Aktif' : 'Tidak aktif' }}</td>
                        <td><a href="{{ url()->route('dashboard.manage.update_datum', ['slug' => $layer->getName(), 'datumId' => $datumLayer->id]) }}">Update</a></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control" name="newDataLayer[name][]" placeholder="Judul datum baru">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <select name="newDataLayer[type][]" class="form-control">
                                @foreach (\App\Modules\Map\Models\DataLayer::getChoosableTypeLabels() as $typeId => $typeLabel)
                                    <option value="{{ $typeId }}">{{ $typeLabel }}</option>
                                @endforeach
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <select name="newDataLayer[is_active][]" class="form-control">
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        <button type="submit" class="btn btn-primary">Submit Pembaharuan</button>
    </form>
</div></div>
<div>&nbsp;</div>
@endsection

@push('scripts')
<script>

</script>
@endpush

<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.wide')

@section('title', 'Map')

@section('content')
    <div id="map-container"></div>
@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = 'kelurahan';

    /**
     * @return mixed
     */
    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'id_kecamatan', 'id');
    }
}
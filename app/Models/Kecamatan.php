<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = 'kecamatan';

    /**
     * Get Kabko.
     * @return mixed
     */
    public function kabko()
    {
        return $this->belongsTo(Kabko::class, 'id_kokab', 'id');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kabko extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = 'kokab';

    /**
     * Get Kabko.
     * @return mixed
     */
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi', 'id');
    }
}
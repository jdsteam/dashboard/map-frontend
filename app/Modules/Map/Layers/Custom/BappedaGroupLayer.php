<?php
/**
 * BappedaGroupLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */

namespace App\Modules\Map\Layers\Custom;

use App\Modules\Map\Layers\AbstractGroupLayer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */
class BappedaGroupLayer extends AbstractGroupLayer
{
    /**
     * @var string
     */
    protected $group_name = 'bappeda';

    /**
     * @var string
     */
    protected $label = 'Bappeda';

    /**
     * @return array
     */
    protected function layerList(): array
    {
        return [
            Bappeda\DesaLumbungPadiLayer::class,
            Bappeda\PertumbuhanKukmLayer::class,
        ];
    }
}
<?php
/**
 * CalonPenerimaBioGasLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.16
 */

namespace App\Modules\Map\Layers\Custom\DinasLingkunganHidup;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\Map\Models\Custom\DinasLingkunganHidup\CalonPenerimaBioGas;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.16
 */
class CalonPenerimaBioGasLayer extends \App\Modules\Map\Layers\AbstractLayer
{
    /**
     * @var string
     */
    protected $layer_name = 'calon_penerima_biogas';

    /**
     * @var string
     */
    protected $label = 'Calon Penerima Bio Gas';

    /**
     * @var string
     */
    protected $type = self::LAYER_ICON;

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return array_merge([
            'iconAtlas' => '/location-icon-atlas.png',
            'iconMapping' => '/location-icon-mapping.json',
        ], parent::getConfig());
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public function getData(Request $request): Collection
    {
        $data = CalonPenerimaBioGas::select(
            'id',
            'nama_penerima',
            \DB::raw('ST_X(ST_TRANSFORM(geometry, 4326)) AS longitude'),
            \DB::raw('ST_Y(ST_TRANSFORM(geometry, 4326)) AS latitude')
        )->get();
        $label = $this->getLabel();

        return $data->map(function ($item) use ($label) {
            return [
                'id' => $item->id,
                'label' => $item->nama_penerima,
                'attributes' => [],
                'coordinates' => [
                    (float) $item->longitude,
                    (float) $item->latitude
                ]
            ];
        });
    }
}
<?php
/**
 * DesaLumbungPadiLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */

namespace App\Modules\Map\Layers\Custom\Bappeda;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\Map\Models\Custom\Bappeda\DesaLumbungPadi;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */
class DesaLumbungPadiLayer extends \App\Modules\Map\Layers\AbstractLayer
{
    /**
     * @var string
     */
    protected $layer_name = 'desa_lumbung_padi';

    /**
     * @var string
     */
    protected $label = 'Desa Lumbung Padi';

    /**
     * @var string
     */
    protected $type = self::LAYER_ICON;

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return array_merge([
            'iconAtlas' => '/location-icon-atlas.png',
            'iconMapping' => '/location-icon-mapping.json',
        ], parent::getConfig());
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public function getData(Request $request): Collection
    {
        $data = DesaLumbungPadi::with(['kelurahan', 'kelurahan.kecamatan', 'kelurahan.kecamatan.kabko'])->select(
            'id',
            'bps_kode',
            \DB::raw('ST_X(ST_TRANSFORM(geometry, 4326)) AS longitude'),
            \DB::raw('ST_Y(ST_TRANSFORM(geometry, 4326)) AS latitude'),
            'keterangan'
        )->get();

        return $data->map(function ($item) {
            return [
                'id' => $item->id,
                'label' => $item->keterangan,
                'attributes' => [
                    'Kode Desa' => $item->bps_kode,
                    'Kel/Desa' => $item->kelurahan ? $item->kelurahan->bps_nama : null,
                    'Kecamatan' => $item->kelurahan ? $item->kelurahan->kecamatan->bps_nama : null,
                    'Kabupaten' => $item->kelurahan ? $item->kelurahan->kecamatan->kabko->bps_nama : null,
                ],
                'coordinates' => [
                    (float) $item->longitude,
                    (float) $item->latitude
                ]
            ];
        });
    }
}
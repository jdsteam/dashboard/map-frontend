<?php
/**
 * KabupatenLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.16
 */

namespace App\Modules\Map\Layers\Custom;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\Map\Layers\AbstractLayer;
use App\Models\Kabko;

/**
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.16
 */
class KabupatenLayer extends AbstractLayer
{
    /**
     * @var string
     */
    protected $layer_name = 'kabupaten';

    /**
     * @var string
     */
    protected $label = 'Kota/Kabupaten';

    /**
     * @var string
     */
    protected $type = self::LAYER_GEOJSON;

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public function getData(Request $request): Collection
    {
        $items = Kabko::select([
            'id',
            'bps_kode',
            'bps_nama',
            \DB::raw('ST_AsGeoJSON(ST_Simplify(ST_SetSRID(polygon, 4326), 0.00005)) AS feature')
        ])->where('id_provinsi', '=', 13)->get();

        // $items = self::filter($request, $items);

        return collect([
            'type' => 'FeatureCollection',
            'crs' => [
                'type' => 'name',
                'properties' => [
                    'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84'
                ]
            ],
            'features' => $items->map(function ($item) {
                return [
                    'id' => $item->id,
                    'type' => 'Feature',
                    'properties' => [
                        'label' => $item->bps_nama,
                        'Kode' => $item->bps_kode
                    ],
                    'geometry' => json_decode($item->feature)
                ];
            }
        )]);
    }

    /**
     * @return Collection
     */
    private static function filter(Request $request, Collection $collection)
    {
        if (!$request->get('attr')) {
            return $collection;
        }

        $formattedRequest = self::reformatRequest($request);

        return $collection->filter(function ($value, $key) use ($formattedRequest) {
            $attributes = $value->getAttributes();
            foreach ($formattedRequest as $item) {
                if ($item['attribute'] == 'Label') {
                    continue;
                } else {
                    foreach ($attributes as $key => $value) {
                        if ($key == $item['attribute']) {
                            switch ($item['operator']) {
                                case '0':
                                    return $value == $item['value'];
                                case '1':
                                    return $value > $item['value'];
                                case '2':
                                    return $value < $item['value'];
                                case '3':
                                    return $value >= $item['value'];
                                case '4':
                                    return $value <= $item['value'];
                                case '5':
                                    return $value != $item['value'];
                                default:
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
        })->values();
    }

    /**
     * @return array
     */
    private static function reformatRequest($request): array
    {
        $attributes = $request->get('attr');
        $operators = $request->get('op');
        $values = $request->get('v');

        $retval = [];
        foreach ($attributes as $i => $attribute) {
            $retval[] = [
                'attribute' => $attribute,
                'operator' => $operators[$i],
                'value' => $values[$i]
            ];
        }

        return $retval;
    }
}
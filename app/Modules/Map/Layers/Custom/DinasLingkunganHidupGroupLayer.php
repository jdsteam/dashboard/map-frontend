<?php
/**
 * DinasLingkunganHidupGroupLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */

namespace App\Modules\Map\Layers\Custom;

use App\Modules\Map\Layers\AbstractGroupLayer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */
class DinasLingkunganHidupGroupLayer extends AbstractGroupLayer
{
    /**
     * @var string
     */
    protected $group_name = 'd_l_h';

    /**
     * @var string
     */
    protected $label = 'Dinas Lingkungan Hidup';

    /**
     * @return array
     */
    protected function layerList(): array
    {
        return [
            DinasLingkunganHidup\CalonPenerimaBioGasLayer::class,
        ];
    }
}
<?php

namespace App\Modules\Map\Layers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

abstract class AbstractLayer implements Layer
{
    use LayerConfigTrait;

    /**
     * @var AbstractGroupLayer|null
     */
    private $_groupLayer;

    /**
     * @param AbstractGroupLayer|null $group Group.
     * @return void
     */
    public function __construct($group)
    {
        if ($group instanceof AbstractGroupLayer) {
            $this->_groupLayer = $group;
        }
    }

    /**
     * @return AbstractGroupLayer|null
     */
    public function getGroup()
    {
        return $this->_groupLayer;
    }

    /**
     * Get group layer name.
     * @return string
     */
    public function getName(): string
    {
        return $this->layer_name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public abstract function getData(Request $request): Collection;
}
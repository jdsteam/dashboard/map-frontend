<?php
/**
 * Layer inteface file.
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */

namespace App\Modules\Map\Layers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */
interface Layer
{
    const LAYER_ICON_ID = 1;
    const LAYER_POLYGON_ID = 2;
    const LAYER_GEOJSON_ID = 3;

    const LAYER_ICON = 'icon-layer';
    const LAYER_POLYGON = 'polygon';
    const LAYER_GEOJSON = 'geojson';
    const LAYER_UNKNOWN = 'unknown';

    /**
     * @return AbstractGroupLayer|null
     */
    public function getGroup();

    /**
     * Get group layer name.
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return array
     */
    public function getConfig(): array;

    /**
     * @return array
     */
    public function getAttributes(): array;

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public function getData(Request $request): Collection;
}
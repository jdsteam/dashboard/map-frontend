<?php

namespace App\Modules\Map\Layers;

use Illuminate\Support\Collection;

abstract class AbstractGroupLayer
{
    /**
     * @param mixed $anything Anything.
     * @return void
     */
    public function __construct($anything)
    {
        $anything; // unused
    }

    /**
     * Get group layer name.
     * @return string
     */
    public function getName(): string
    {
        return $this->group_name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getLayerList(): array
    {
        return $this->getLayerObjects()
            ->mapWithKeys(function ($layer) {
                return [$layer->getName() => [
                    'label' => $layer->getLabel()
                ]];
            })->all();
    }

    /**
     * @var null|Collection
     */
    private $_layerObjects;

    /**
     * @return Collection
     */
    public function getLayerObjects(): Collection
    {
        if ($this->_layerObjects) {
            return $this->_layerObjects;
        }

        $layerList = collect($this->layerList());
        $self = $this;
        return $this->_layerObjects = $layerList->mapWithKeys(function ($className) use ($self) {
            $layer = new $className($self);
            return [$layer->getName() => $layer];
        });
    }

    /**
     * Get list of layer group.
     * @return array
     */
    protected abstract function layerList(): array;

    /**
     * @param string $layerName Layer name.
     * @return null|AbstractLayer
     */
    public function getLayer(string $layerName)
    {
        return $this->getLayerObjects()->get($layerName);
    }

    /**
     * @param string $layerName Layer name.
     * @return array|null
     */
    public function getConfigByLayerName(string $layerName)
    {
        $layerList = $this->getLayerObjects();
        if ($layerList->has($layerName)) {
            return $layerList->get($layerName)->getConfig();
        }
    }
}
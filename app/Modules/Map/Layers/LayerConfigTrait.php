<?php

namespace App\Modules\Map\Layers;

trait LayerConfigTrait
{
    /**
     * @return array
     */
    public function getConfig(): array
    {
        if (!$this->getGroup()) {
            return [
                'id' => md5($this->getName()),
                'title' => $this->getLabel(),
                'data' => url()->route('dashboard.data-single', [
                    'groupName' => $this->getName()
                ])
            ];
        }

        return [
            'id' => md5("{$this->getGroup()->getName()}{$this->getName()}"),
            'title' => $this->getLabel(),
            'data' => url()->route('dashboard.data', [
                'groupName' => $this->getGroup()->getName(),
                'layerName' => $this->getName(),
            ])
        ];
    }
}
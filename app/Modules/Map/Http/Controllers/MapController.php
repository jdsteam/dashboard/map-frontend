<?php
/**
 * MapController class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */

namespace App\Modules\Map\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller as BaseController;
use App\Modules\Map\Layers\Layer as LayerInterface;
use App\Modules\Map\Models\Layer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.15
 */
class MapController extends BaseController
{
    /**
     * @return void
     */
    public function __constuct()
    {
        // Nothing
    }

    /**
     * Index view for map.
     * @return mixed
     */
    public function doView()
    {
        return view('map.view');
    }

    /**
     * @return Response
     */
    public function doGetList(Request $request)
    {
        $list = static::getGroupObjects()->merge(self::layerGroupListFromDatabase());
        return response()->json($list->mapWithKeys(function ($group) {
            if ($group instanceof LayerInterface) {
                return [$group->getName() => [
                    'label' => $group->getLabel(),
                ]];
            }

            return [$group->getName() => [
                'label' => $group->getLabel(),
                'layers' => $group->getLayerList()
            ]];
        })->all());
    }

    public function doGetConfigSingle($groupName)
    {
        $layer = Layer::where(['slug' => $groupName])->first();
        if (!$layer) {
            $layer = static::getGroupObjects()->get($groupName);
        }

        if ($layer) {
            return [
                'type' => $layer->getType(),
                'config' => $layer->getConfig(),
                'attributeTypes' => $layer->getAttributes(),
            ];
        }

        abort(404);
    }

    /**
     * @return Response
     */
    public function doGetConfig($groupName, $layerName)
    {
        $layer = static::getLayerObject($groupName, $layerName);

        if ($layer) {
            return [
                'type' => $layer->getType(),
                'config' => $layer->getConfig(),
            ];
        }

        abort(404);
    }

    /**
     * @param string $groupName Group name.
     * @param Request $request Request object.
     * @return Response
     */
    public function doGetDataSingle($groupName, Request $request)
    {
        $layer = Layer::where(['slug' => $groupName])->first();
        if (!$layer) {
            $layer = static::getGroupObjects()->get($groupName);
        }

        if ($layer) {
            return $layer->getData($request);
        }

        abort(404);
    }

    /**
     * @return Response
     */
    public function doGetData($groupName, $layerName, Request $request)
    {
        $layer = static::getLayerObject($groupName, $layerName);

        if ($layer) {
            return $layer->getData($request);
        }

        abort(404);
    }

    /**
     * @return null|\App\Modules\Map\Layers\AbstractLayer
     */
    protected static function getLayerObject($groupName, $layerName)
    {
        $groups = static::getGroupObjects();

        if ($groups->has($groupName)) {
            return $groups->get($groupName)->getLayer($layerName);
        }
        return null;
    }

    /**
     * @return Collection
     */
    protected static function getGroupObjects(): Collection
    {
        return collect(self::layerGroupList())->mapWithKeys(function ($className) {
            $group = new $className(null);
            /** @var \App\Modules\Map\Layers\AbstractGroupLayer $group */
            return [$group->getName() => $group];
        });
    }

    /**
     * @return Collection
     */
    private static function layerGroupListFromDatabase(): Collection
    {
        return Layer::where(['is_active' => true])->get()->mapWithKeys(function ($layer) {
            return [$layer->getName() => $layer];
        });
    }

    /**
     * @return array
     */
    private static function layerGroupList(): array
    {
        return config('jds.layer_group');
    }
}
<?php
/**
 * ManageController class file.
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */

namespace App\Modules\Map\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use App\Modules\Map\Models\DataAttributeLayer;
use App\Modules\Map\Models\SingleDataLayer;
use App\Modules\Map\Models\DataLayer;
use App\Modules\Map\Models\SingleLayer;
use App\Modules\Map\Models\Layer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */
class ManageController extends BaseController
{
    /**
     * @return Response
     */
    public function doView()
    {
        return view('manage.view', [
            'layers' => Layer::all()
        ]);
    }

    /**
     * @param Request $request Request.
     * @return Response
     */
    public function doAddLayer(Request $request)
    {
        foreach(self::reformatNewLayer($request) as $updateAttribute) {
            $attribute = new SingleLayer();
            foreach($updateAttribute as $key => $value) {
                $attribute->{$key} = $value;
            }
            $attribute->save();
        }

        return $this->doView();
    }

    /**
     * @return Response
     */
    public function doViewUpdate($slug)
    {
        $layer = Layer::where('slug', '=', $slug)->first();

        return view('manage.update', [
            'layer' => $layer,
            'dataLayer' => DataLayer::where('id_layer', $layer->id)->get(),
        ]);
    }

    public function doUpdate($slug, Request $request)
    {
        if ($detail = $request->get('detail')) {
            $layer = SingleLayer::where('slug', '=', $slug)->first();
            foreach($detail as $detailKey => $detailValue) {
                $layer->{$detailKey} = $detailValue;
            }
            $layer->save();
        } else {
            $layer = Layer::where('slug', '=', $slug)->first();

            foreach(self::reformatNewDataLayer($request) as $updateAttribute) {
                $dataLayer = new SingleDataLayer();
                $dataLayer->id_layer = $layer->id;
                foreach($updateAttribute as $key => $value) {
                    $dataLayer->{$key} = $value;
                }
                $dataLayer->save();
            }
        }

        return redirect()->route('dashboard.manage.update', [
            'slug' => $request->get('detail')['slug'] ?? $slug,
        ]);
    }

    public function doAddNewData($slug, Request $request)
    {
        $layer = Layer::where('slug', '=', $slug)->first();

        if ($request->hasFile('datafile')) {
            $file = fopen($request->file('datafile')->getRealPath(), 'r');
            $headers = array_flip(fgetcsv($file, 0, ','));

            while(!feof($file)) {
                $row = fgetcsv($file, 0, ',');
                $dataLayer = new SingleDataLayer();
                $dataLayer->id_layer = $layer->id;
                $dataLayer->name = $row[$headers['name']];
                $dataLayer->is_active = $row[$headers['is_active']];
                $dataLayer->type = $row[$headers['type']];
                $dataLayer->save();

                foreach($headers as $header => $index) {
                    if (!in_array($header, ['name', 'is_active', 'type'])) {
                        $attribute = new DataAttributeLayer();
                        $attribute->id_data_layer = $dataLayer->id;
                        $attribute->name = $header;
                        $attribute->type = is_int($row[$headers[$header]]) ? DataAttributeLayer::TYPE_INTEGER : DataAttributeLayer::TYPE_STRING;
                        $attribute->value = $row[$headers[$header]];
                        $attribute->save();
                    }
                }
            }

            fclose($file);
        }

        return redirect()->route('dashboard.manage.update', [
            'slug' => $layer->slug
        ]);
    }

    /**
     * @return Response
     */
    public function doViewUpdateDatum($slug, $datumId, Request $request)
    {
        $layer = Layer::where('slug', '=', $slug)->first();

        return view('manage.update-datum', [
            'layer' => $layer,
            'datumLayer' => DataLayer::where('id', $datumId)->first(),
            'attributes' => DataAttributeLayer::where('id_data_layer', $datumId)->get(),
        ]);
    }

    /**
     * @return Response
     */
    public function doUpdateDatum($slug, $datumId, Request $request)
    {
        if ($detail = $request->get('detail')) {
            $dataLayer = SingleDataLayer::findOrFail($datumId);
            foreach($detail as $detailKey => $detailValue) {
                $dataLayer->{$detailKey} = $detailValue;
            }
            $dataLayer->save();
        } else {
            foreach($request->get('attribute', []) as $updateId => $updateAttribute) {
                $attribute = DataAttributeLayer::findOrFail($updateId);
                foreach($updateAttribute as $key => $value) {
                    $attribute->{$key} = $value;
                }
                $attribute->save();
            }

            foreach(self::reformatNewAttribute($request) as $updateAttribute) {
                $attribute = new DataAttributeLayer();
                $attribute->id_data_layer = $datumId;
                foreach($updateAttribute as $key => $value) {
                    $attribute->{$key} = $value;
                }
                $attribute->save();
            }
        }

        return $this->doViewUpdateDatum($slug, $datumId, $request);
    }

    /**
     * @return Response
     */
    public function doDeleteAttributeDatum($slug, $datumId, $attribteId)
    {
        $attribute = DataAttributeLayer::find($attribteId);
        $attribute->delete();

        return redirect()->route('dashboard.manage.update_datum', [
            'slug' => $slug,
            'datumId' => $datumId
        ]);
    }

    /**
     * @return array
     */
    private static function reformatNewLayer(Request $request): array
    {
        $newAttribute = $request->get('newLayer');
        if (!$newAttribute) {
            return [];
        }

        $length = count($newAttribute['name']);

        $retval = [];
        for ($i=0; $i < $length; $i++) {
            if (empty($newAttribute['name'][$i])) {
                continue;
            }

            $retval[] = [
                'name' => $newAttribute['name'][$i],
                'slug' => $newAttribute['slug'][$i],
                'type' => $newAttribute['type'][$i]
            ];
        }

        return $retval;
    }

    /**
     * @return array
     */
    private static function reformatNewDataLayer(Request $request): array
    {
        $newAttribute = $request->get('newDataLayer');
        if (!$newAttribute) {
            return [];
        }

        $length = count($newAttribute['name']);

        $retval = [];
        for ($i=0; $i < $length; $i++) {
            if (empty($newAttribute['name'][$i])) {
                continue;
            }

            $retval[] = [
                'name' => $newAttribute['name'][$i],
                'type' => $newAttribute['type'][$i],
                'is_active' => $newAttribute['is_active'][$i]
            ];
        }

        return $retval;
    }

    /**
     * @return array
     */
    private static function reformatNewAttribute(Request $request): array
    {
        $newAttribute = $request->get('newAttribute');
        if (!$newAttribute) {
            return [];
        }

        $length = count($newAttribute['name']);

        $retval = [];
        for ($i=0; $i < $length; $i++) {
            if (empty($newAttribute['name'][$i])) {
                continue;
            }

            $retval[] = [
                'name' => $newAttribute['name'][$i],
                'type' => $newAttribute['type'][$i],
                'value' => $newAttribute['value'][$i]
            ];
        }

        return $retval;
    }
}
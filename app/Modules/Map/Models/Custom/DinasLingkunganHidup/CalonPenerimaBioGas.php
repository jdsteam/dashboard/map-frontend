<?php

namespace App\Modules\Map\Models\Custom\DinasLingkunganHidup;

use Illuminate\Database\Eloquent\Model;

class CalonPenerimaBioGas extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = 'dlh_calonpenerimabiogas';
}
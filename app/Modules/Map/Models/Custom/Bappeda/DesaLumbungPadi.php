<?php

namespace App\Modules\Map\Models\Custom\Bappeda;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kelurahan;

class DesaLumbungPadi extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = 'bappeda_desalumbungpadi';

    /**
     * @return mixed
     */
    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'bps_kode', 'bps_kode');
    }
}
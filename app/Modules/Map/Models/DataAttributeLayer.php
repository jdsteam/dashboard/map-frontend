<?php
/**
 * DataAttributeLayer class file.
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */

namespace App\Modules\Map\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */
class DataAttributeLayer extends Model
{
    const TYPE_INTEGER = 1;
    const TYPE_STRING = 2;

    use SoftDeletes;

    /**
     * Table name.
     * @var string
     */
    protected $table = 'data_attribute_layer';

    /**
     * @return mixed
     */
    public function dataLayer()
    {
        return $this->belongsTo(DataLayer::class);
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTypeLabel(): string
    {
        return self::getTypeLabels()[$this->type];
    }

    /**
     * @return array
     */
    public static function getTypeLabels(): array
    {
        return [
            self::TYPE_INTEGER => 'Integer',
            self::TYPE_STRING => 'String'
        ];
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_INTEGER,
            self::TYPE_STRING
        ];
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        switch ($this->type) {
            case self::TYPE_INTEGER:
                return (int) $this->value;
            case self::TYPE_STRING:
                return $this->value;
        }
        throw new \Exception('Data attribute type not found');
    }
}
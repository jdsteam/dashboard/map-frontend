<?php
/**
 * DataLayer class file.
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */

namespace App\Modules\Map\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use App\Models\Kabko;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */
class DataLayer extends Model
{
    const TYPE_CUSTOM = 0;
    const TYPE_POL_KABKO = 1;
    const TYPE_POL_KECAMATAN = 2;
    const TYPE_POL_DESA = 3;
    const TYPE_CUSTOM_POINT = 4;

    use SoftDeletes;

    /**
     * Table name.
     * @var string
     */
    protected $table = 'data_layer';

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTypeLabel(): string
    {
        return static::getTypeLabels()[$this->type];
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        if ($this->type == self::TYPE_POL_KABKO) {
            return $this->attributeLayers->filter(function ($value, $key) {
                return $key != 'kokab_id';
            })->mapWithKeys(function ($item) {
                return [$item->getLabel() => $item->getValue()];
            })->all();
        }
        return [];
    }

    /**
     * @return array
     */
    public function getAttributeTypes(): array
    {
        if ($this->type == self::TYPE_POL_KABKO) {
            return $this->attributeLayers->filter(function ($value, $key) {
                return $key != 'kokab_id';
            })->map(function ($item) {
                return [
                    'label' => $item->getLabel(),
                    'type' => $item->type,
                ];
            })->all();
        }
        return [];
    }

    /**
     * @return array
     */
    public function getFeature(): array
    {
        if ($this->type == self::TYPE_POL_KABKO) {
            return json_decode($this->getKabkoPolygon()->feature, true);
        }
        return [];
    }

    /**
     * @var null|Kabko
     */
    private $_pol_kabko;

    /**
     * @return Kabko
     */
    public function getKabkoPolygon(): Kabko
    {
        if ($this->_pol_kabko) {
            return $this->_pol_kabko;
        }

        return $this->_pol_kabko = Kabko::select([
            'id',
            'bps_kode',
            'bps_nama',
            \DB::raw('ST_AsGeoJSON(ST_Simplify(ST_SetSRID(polygon, 4326), 0.00005)) AS feature')
        ])->where(['id' => $this->getAttributeLayer('kokab_id')])->first();
    }

    /**
     * Get all of the posts for the user.
     */
    public function attributeLayers()
    {
        return $this->hasMany(DataAttributeLayer::class, 'id_data_layer', 'id');
    }

    /**
     * @return mixed
     */
    public function getAttributeLayer($name)
    {
        $attributeLayers = $this->attributeLayersByKey();
        if ($attributeLayers && isset($attributeLayers[$name])) {
            return $attributeLayers[$name]->getValue();
        }

        return null;
    }

    /**
     * @var null|Collection
     */
    private $_attribute_layers_by_key;

    /**
     *
     */
    public function attributeLayersByKey(): Collection
    {
        if ($this->_attribute_layers_by_key) {
            return $this->_attribute_layers_by_key;
        }

        return $this->_attribute_layers_by_key = $this->attributeLayers->mapWithKeys(function ($attributeLayer) {
            return [$attributeLayer->getLabel() => $attributeLayer];
        });
    }

    public static function getChoosableTypeLabels(): array
    {
        return [
            self::TYPE_POL_KABKO => 'Geometry Kabko',
            self::TYPE_POL_KECAMATAN => 'Geometry Kecamatan',
            self::TYPE_POL_DESA => 'Geometry Desa',
        ];
    }

    /**
     * @return array
     */
    public static function getTypeLabels(): array
    {
        return [
            self::TYPE_POL_KABKO => 'Geometry Kabko',
            self::TYPE_POL_KECAMATAN => 'Geometry Kecamatan',
            self::TYPE_POL_DESA => 'Geometry Desa',
            self::TYPE_CUSTOM_POINT => 'Titik/Point'
        ];
    }
}
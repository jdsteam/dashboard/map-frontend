<?php
/**
 * Layer class file.
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */

namespace App\Modules\Map\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\Map\Layers\Layer as LayerInterface;
use App\Modules\Map\Layers\LayerConfigTrait;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.18
 */
class Layer extends Model implements LayerInterface
{
    use SoftDeletes;
    use LayerConfigTrait;

    /**
     * Table name.
     * @var string
     */
    protected $table = 'layer';

    /**
     * @return AbstractGroupLayer
     */
    public function getGroup()
    {
        return null;
    }

    /**
     * Get group layer name.
     * @return string
     */
    public function getName(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        $types = static::getAvailableTypes();

        if (!isset($types[$this->type])) {
            return self::LAYER_UNKNOWN;
        }

        return $types[$this->type];
    }

    /**
     * @return string
     */
    public function getTypeLabel(): string
    {
        return static::getTypeLabels()[$this->type];
    }

    /**
     * @return array
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::LAYER_ICON_ID => self::LAYER_ICON,
            self::LAYER_GEOJSON_ID => self::LAYER_GEOJSON
        ];
    }

    /**
     * @return array
     */
    public static function getTypeLabels(): array
    {
        return [
            self::LAYER_ICON_ID => 'Icon',
            self::LAYER_GEOJSON_ID => 'GeoJSON'
        ];
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $class = $this->getSupportLayerClass();
        $supportLayer = new $class($this);

        return $supportLayer->getAttributes();
    }

    /**
     * @param Request $request Request.
     * @return Collection
     */
    public function getData(Request $request): Collection
    {
        $class = $this->getSupportLayerClass();
        $supportLayer = new $class($this);

        return $supportLayer->getMapData($request);
    }

    /**
     * @return string
     */
    private function getSupportLayerClass(): string
    {
        $supportLayers = [
            self::LAYER_ICON_ID => \App\Modules\Map\Support\Layers\IconLayer::class,
            self::LAYER_GEOJSON_ID => \App\Modules\Map\Support\Layers\GeoJsonLayer::class
        ];

        return $supportLayers[$this->type];
    }

    /**
     * Get all of the posts for the user.
     */
    public function dataLayers()
    {
        return $this->hasMany(DataLayer::class, 'id_layer', 'id');
    }
}
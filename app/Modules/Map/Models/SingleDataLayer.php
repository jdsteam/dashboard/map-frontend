<?php
/**
 * SingleDataLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.21
 */

namespace App\Modules\Map\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.21
 */
class SingleDataLayer extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     * @var string
     */
    protected $table = 'data_layer';
}
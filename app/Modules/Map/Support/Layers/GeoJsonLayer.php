<?php
/**
 * GeoJsonLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.22
 */

namespace App\Modules\Map\Support\Layers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\Map\Models\DataAttributeLayer;
use App\Modules\Map\Models\Layer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.22
 */
class GeoJsonLayer
{
    /**
     * @var Layer
     */
    private $_layer;

    /**
     * @return void
     */
    public function __construct(Layer $layer)
    {
        $this->_layer = $layer;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $dataLayer = $this->_layer->dataLayers()->with('attributeLayers')->select([
            'id',
            'type',
            'name'
        ])->where(['is_active' => true])->first();

        return collect(array_merge([
            [
                'label' => 'Label',
                'type' => DataAttributeLayer::TYPE_STRING
            ],
        ], $dataLayer->getAttributeTypes()))->all();
    }

    /**
     * @param Request $request Request object.
     * @return Collection
     */
    public function getMapData(Request $request): Collection
    {
        $dataLayers = $this->_layer->dataLayers()->with('attributeLayers')->select([
            'id',
            'type',
            'name'
        ])->where(['is_active' => true])->get();

        $dataLayers = self::filter($request, $dataLayers);

        return collect([
            'type' => 'FeatureCollection',
            'crs' => [
                'type' => 'name',
                'properties' => [
                    'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84'
                ]
            ],
            'features' => $dataLayers->map(function ($dataLayer) {
                return [
                    'id' => $dataLayer->id,
                    'type' => 'Feature',
                    'properties' => array_merge([
                        'label' => $dataLayer->getLabel(),
                    ], $dataLayer->getAttributes()),
                    'geometry' => $dataLayer->getFeature()
                ];
            }
        )]);
    }

    /**
     * @return Collection
     */
    private static function filter(Request $request, Collection $collection)
    {
        if (!$request->get('attr')) {
            return $collection;
        }

        $formattedRequest = self::reformatRequest($request);

        return $collection->filter(function ($value, $key) use ($formattedRequest) {
            $attributes = $value->getAttributes();
            foreach ($formattedRequest as $item) {
                if ($item['attribute'] == 'Label') {
                    continue;
                } else {
                    foreach ($attributes as $key => $value) {
                        if ($key == $item['attribute']) {
                            switch ($item['operator']) {
                                case '0':
                                    return $value == $item['value'];
                                case '1':
                                    return $value > $item['value'];
                                case '2':
                                    return $value < $item['value'];
                                case '3':
                                    return $value >= $item['value'];
                                case '4':
                                    return $value <= $item['value'];
                                case '5':
                                    return $value != $item['value'];
                                default:
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
        })->values();
    }

    /**
     * @return array
     */
    private static function reformatRequest($request): array
    {
        $attributes = $request->get('attr');
        $operators = $request->get('op');
        $values = $request->get('v');

        $retval = [];
        foreach ($attributes as $i => $attribute) {
            $retval[] = [
                'attribute' => $attribute,
                'operator' => $operators[$i],
                'value' => $values[$i]
            ];
        }

        return $retval;
    }
}
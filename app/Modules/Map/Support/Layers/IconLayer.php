<?php
/**
 * IconLayer class file.
 *
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.22
 */

namespace App\Modules\Map\Support\Layers;

use Illuminate\Support\Collection;
use App\Modules\Map\Models\DataAttributeLayer;
use App\Modules\Map\Models\Layer;

/**
 * @author Setyo Legowo <gw.tio145@gmail.com>
 * @since 2019.02.22
 */
class IconLayer
{
    /**
     * @var Layer
     */
    private $_layer;

    /**
     * @return void
     */
    public function __construct(Layer $layer)
    {
        $this->_layer = $layer;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $dataLayer = $this->_layer->dataLayers()->with('attributeLayers')->select([
            'id',
        ])->where(['is_active' => true])->first();

        // TODO
        return array_merge([
            [
                'label' => 'Label',
                'type' => DataAttributeLayer::TYPE_STRING
            ]
        ], $dataLayer->getAttributeTypes());
    }

    /**
     * @return Collection
     */
    public function getMapData(): Collection
    {
        $dataLayers = $this->_layer->dataLayers()->with('attributeLayers')->select([
            'id',
            'name',
            \DB::raw('ST_X(ST_TRANSFORM(geometry, 4326)) AS longitude'),
            \DB::raw('ST_Y(ST_TRANSFORM(geometry, 4326)) AS latitude')
        ])->where(['is_active' => true])->get();

        return $dataLayers->map(function ($dataLayer) {
            return [
                'id' => $dataLayer->id,
                'label' => $dataLayer->getLabel(),
                'attributes' => $dataLayer->attributeLayers->all(),
                'coordinates' => [
                    (float) $item->longitude,
                    (float) $item->latitude
                ],
            ];
        });
    }

    /**
     * @return Collection
     */
    private static function filter(Request $request, Collection $collection)
    {
        if (!$request->get('attr')) {
            return $collection;
        }

        $formattedRequest = self::reformatRequest($request);

        return $collection->filter(function ($value, $key) use ($formattedRequest) {
            $attributes = $value->getAttributes();
            foreach ($formattedRequest as $item) {
                if ($item['attribute'] == 'Label') {
                    continue;
                } else {
                    foreach ($attributes as $key => $value) {
                        if ($key == $item['attribute']) {
                            switch ($item['operator']) {
                                case '0':
                                    return $value == $item['value'];
                                case '1':
                                    return $value > $item['value'];
                                case '2':
                                    return $value < $item['value'];
                                case '3':
                                    return $value >= $item['value'];
                                case '4':
                                    return $value <= $item['value'];
                                case '5':
                                    return $value != $item['value'];
                                default:
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
        })->values();
    }

    /**
     * @return array
     */
    private static function reformatRequest($request): array
    {
        $attributes = $request->get('attr');
        $operators = $request->get('op');
        $values = $request->get('v');

        $retval = [];
        foreach ($attributes as $i => $attribute) {
            $retval[] = [
                'attribute' => $attribute,
                'operator' => $operators[$i],
                'value' => $values[$i]
            ];
        }

        return $retval;
    }
}
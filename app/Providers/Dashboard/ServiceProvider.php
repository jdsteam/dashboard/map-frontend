<?php

namespace App\Providers\Dashboard;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as ParentServiceProvider;

class ServiceProvider extends ParentServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupRouting();

        // setup routing
    }

    private function setupRouting()
    {
        $prefix = config('jds.route_prefix') ?: 'api/map/';

        Route::group([
            'middleware' => ['web'],
            'namespace' => 'App\Modules\Map\Http\Controllers'
        ], function() {
            Route::get('/map-view', 'MapController@doView')->name('dashboard.view');
        });

        Route::group([
            'prefix' => 'manage/',
            'middleware' => ['web'],
            'namespace' => 'App\Modules\Map\Http\Controllers'
        ], function() {
            Route::get('/', 'ManageController@doView')->name('dashboard.manage.view');
            Route::post('/', 'ManageController@doAddLayer');
            Route::get('/update/{slug}', 'ManageController@doViewUpdate')->name('dashboard.manage.update');
            Route::post('/update/{slug}', 'ManageController@doUpdate');
            Route::post('/update/{slug}/add-data', 'ManageController@doAddNewData')->name('dashboard.manage.add-data');
            Route::get('/update/{slug}/delete', 'ManageController@doUpdate')->name('dashboard.manage.delete-layer');
            Route::get('/update/{slug}/datum-update/{datumId}', 'ManageController@doViewUpdateDatum')->name('dashboard.manage.update_datum');
            Route::post('/update/{slug}/datum-update/{datumId}', 'ManageController@doUpdateDatum');
            Route::get('/update/{slug}/datum-update/{datumId}/delete', 'ManageController@doDeleteDatum')->name('dashboard.manage.delete_datum');
            Route::get('/update/{slug}/datum-update/{datumId}/{attributeId}/delete', 'ManageController@doDeleteAttributeDatum')->name('dashboard.manage.delete_attribute_datum');
        });

        Route::group([
            'prefix' => $prefix,
            'middleware' => ['api'],
            'namespace' => 'App\Modules\Map\Http\Controllers'
        ], function() {
            Route::get('/health', function() { return 'OK'; });
            Route::get('/get-list', 'MapController@doGetList');
            Route::get('/layer-config/{groupName}', 'MapController@doGetConfigSingle');
            Route::get('/layer-config/{groupName}/{layerName}', 'MapController@doGetConfig');
            Route::get('/data/{groupName}', 'MapController@doGetDataSingle')->name('dashboard.data-single');
            Route::get('/data/{groupName}/{layerName}', 'MapController@doGetData')->name('dashboard.data');
        });
    }
}
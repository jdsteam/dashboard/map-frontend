<?php
/**
 * MapController class file.
 *
 * @author Setyo Legowo <setyolegowo94@gmail.com>
 * @since 2019.01.04
 */

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

/**
 * Controller for manage MAP view.
 *
 * @author Setyo Legowo <setyolegowo94@gmail.com>
 * @since 2019.01.04
 */
class MapController extends BaseController
{
    /**
     * Index view for map.
     * @return mixed
     */
    public function actionView()
    {
        return view('map.view');
    }
}
